﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Asp1.Models
{
    public class Elektronik
    {
        public int ID { get; set; }
        public String Merek { get; set; }
        public decimal Harga { get; set; }
        public String Jenis { get; set; }
        public String Warna { get; set; }
    }
}