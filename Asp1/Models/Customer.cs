﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Asp1.Models
{
    public class Customer
    {
        public int ID { get; set; }
        public String Nama { get; set; }
        public decimal NoKTP { get; set; }
        public String Alamat { get; set; }
        public int NoHP { get; set; }
        public String TanggalLahir { get; set; }
    }
}