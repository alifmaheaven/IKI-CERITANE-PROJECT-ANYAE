﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Asp1.Models;

namespace Asp1.Controllers
{
    public class ElektronikController : Controller
    {
        // GET: Elektronik
        public ActionResult Tampil()
        {
            var barang1 = new Elektronik()
            {
                ID = 123,
                Merek = "samsung",
                Harga = 3000000,
                Jenis = "TV",
                Warna = "Hitam"
            };
            return View(barang1);
        }
    }
}