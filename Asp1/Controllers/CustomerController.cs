﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Asp1.Models;

namespace Asp1.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Customer()
        {
            var beli1 = new Customer()
            {
                ID = 12345678,
                Nama = "Alif",
                NoKTP = 12345678,
                Alamat = "Jl.Danau Buyan",
                NoHP = 98765432,
                TanggalLahir = "1 Februari 2018"
            };
            return View(beli1);
        }
    }
}