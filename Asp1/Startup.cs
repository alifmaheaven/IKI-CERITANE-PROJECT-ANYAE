﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Asp1.Startup))]
namespace Asp1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
